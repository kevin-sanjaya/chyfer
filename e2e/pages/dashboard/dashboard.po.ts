import { browser, by, element, promise, ElementFinder, ElementArrayFinder } from 'protractor';

export class DashboardPage {

    navigateToDashboard(): promise.Promise<any> {
        return browser.get('/');
    }

    getHeader(): promise.Promise<string> {
        return element(by.css('.card-title')).getText();
    }

    getSideInfoCount(): promise.Promise<number> {
        return element.all(by.css('.alert')).count();
    }

    getSpeedChartButton(): ElementFinder {
        return element(by.css('.speed-button'));
    }

    getDistanceChartButton(): ElementFinder {
        return element(by.css('.distance-button'));
    }

    getSpeedChartModal(): ElementFinder {
        return element(by.css('.speed-modal'));
    }

    getDistanceChartModal(): ElementFinder {
        return element(by.css('.distance-modal'));
    }
}
