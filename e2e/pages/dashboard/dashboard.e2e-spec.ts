import { browser } from 'protractor';
import { DashboardPage } from './dashboard.po';

describe('Dashboard Page', () => {
    const dashboardPage = new DashboardPage();
    browser.ignoreSynchronization = true;

    beforeEach(() => {
        dashboardPage.navigateToDashboard();
    });

    it('Should have header', () => {
        expect(dashboardPage.getHeader()).toEqual('Chyfer 1.0.0 - Overview');
    });

    it('Should have 7 side information', () => {
        expect(dashboardPage.getSideInfoCount()).toEqual(7);
    });

    it('Should open up a modal when speed chart button is clicked', () => {
        const chartButton = dashboardPage.getSpeedChartButton();
        chartButton.click().then(() => {
            setTimeout(() => expect(dashboardPage.getSpeedChartModal().isDisplayed()).toBeTruthy(), 10000);
        });
    });

    it('Should open up a modal when speed distance button is clicked', () => {
        const chartButton = dashboardPage.getDistanceChartButton();
        chartButton.click().then(() => {
            setTimeout(() => expect(dashboardPage.getDistanceChartModal().isDisplayed()).toBeTruthy(), 10000);
        });
    });
});
