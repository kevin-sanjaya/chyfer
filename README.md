# Chyfer

Chyfer is a POC web application for dashcam data management and visualization. Chyfer takes car trip data sets (JSON data sets that consist of longitude, latitude, distance and speed) as an input and then transforms it into human-readable data and visualize the data using map and chart. This application is a coding challenge from comma.ai https://jobs.lever.co/comma/c1a71f26-d940-4c85-92c5-87d6270bc95d

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Lint

Run `npm run lint` to run lint check on the project.

## Unit testing

Run `npm run test` to run unit-test for every component in the project.

## e2e Testing

Run `npm run e2e` to run e2e automated browser testing.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
