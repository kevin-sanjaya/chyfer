import { Component, OnInit } from '@angular/core';
import { TripService } from '../services/trip.service';
import { Trip } from '../models/trip';
declare let L;
declare let Plotly;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private interactiveMap: any; // Leaflet Map
  private leafletMap: any = L; // Leaflet Library

  private tripData: Trip[];
  public totalDataProcessed = 0;
  public speedDistribution: number[] = [];
  public distanceDistribution: number[] = [];

  public totalTripDistance = 0;
  public totalTripSpeed = 0;

  public isLoading = true;

  constructor(private tripService: TripService) { }

  ngOnInit() {
    this.tripService.getTripData().subscribe((data: Trip[]) => {
      this.tripData = data.map(trip => new Trip(trip));
      this.renderMap();
      this.isLoading = false;
    });
  }

  public get totalTrip(): number {
    return this.tripData ? this.tripData.length : 0;
  }

  public get averageTripDistance(): number {
    return this.tripData ? this.totalTripDistance / this.tripData.length : 0;
  }

  public get averageTripSpeed(): number {
    return this.tripData ? this.totalTripSpeed / this.totalDataProcessed : 0;
  }

  public get firstTripDate(): string {
    return this.tripData ? this.tripData[0].startTime : '';
  }

  public get lastTripDate(): string {
    return this.tripData ? this.tripData[this.tripData.length - 1].startTime : '';
  }

  private renderMap(): void {
    this.interactiveMap = this.leafletMap.map('interactive-map');
    this.leafletMap.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'YOUR_ACCESS_TOKEN_HERE'
    }).addTo(this.interactiveMap);

    this.setMapCoordinate();
    this.printTripOnMap();
    this.renderChart(this.speedDistribution, 'Speed');
    this.renderChart(this.distanceDistribution, 'Distance');
  }

  private setMapCoordinate(): void {
    this.interactiveMap.setView([this.tripData[0].coordinates[0].lattitude, this.tripData[0].coordinates[0].longitude], 10);
  }

  private printTripOnMap(): void {
    let latlngs = [];
    let distance = [];
    let distanceDifference;

    for (const trip of this.tripData) {
      distance = [];

      trip.coordinates.forEach((coords, index) => {
        latlngs.push([coords.lattitude, coords.longitude]);
        if (coords.speeds >= 1) {
          this.speedDistribution.push(coords.speeds);
          this.totalTripSpeed += coords.speeds;
        }
        this.totalDataProcessed++;
        distance.push(coords.distance);
      });

      distanceDifference = Math.max(...distance) - Math.min(...distance);
      this.distanceDistribution.push(distanceDifference);
      this.totalTripDistance += distanceDifference;

      this.leafletMap.polyline(latlngs, { color: '#343A40', weight: 0.8 }).addTo(this.interactiveMap);
      latlngs = [];
    }
  }

  private renderChart(distributionData: number[], type: string): void {
    const chartData = [{
      x: distributionData,
      autobinx: false,
      histnorm: 'count',
      marker: {
        color: 'rgba(52, 58, 64, 1)',
        line: {
          color: 'rgba(52, 58, 64, 1)',
          width: 2
        }
      },
      opacity: 0.5,
      type: 'histogram',
    }];

    const x = type === 'Speed' ? `${type} (Km/h)` : `${type} (Km)`;

    const layout = {
      bargap: 0.5,
      bargroupgap: 0.5,
      barmode: 'overlay',
      title: `Overall ${type} Distribution`,
      xaxis: { title: x },
      yaxis: { title: 'Count' },
      width: 1100,
      height: 700,
    };

    type === 'Speed' ? Plotly.newPlot('speed-distribution', chartData, layout) :
      Plotly.newPlot('distance-distribution', chartData, layout);
  }
}
