import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin } from 'rxjs';
import { Trip } from '../models/trip';

@Injectable({
  providedIn: 'root'
})
export class TripService {
  constructor(private http: HttpClient) { }

  public getTripData(): Observable<Object[]> {
    const trip: Observable<Object>[] = [];

    for (let i = 1; i <= 527; i++) {
      trip.push(this.http.get(`./assets/mocks/trip (${i}).json`));
    }
    return forkJoin(trip);
  }

}
