export class TripCoordinate {
    private lat: number;
    private lng: number;
    private speed: number;
    private dist: number;

    constructor(coords: TripCoordinate) {
        const props = ['lat', 'lng', 'speed', 'dist'];
        props.forEach(prop => this[prop] = coords[prop]);
    }

    public get longitude(): number {
        return this.lng;
    }

    public get lattitude(): number {
        return this.lat;
    }

    public get speeds(): number {
        return this.speed;
    }

    public get distance(): number {
        return this.dist;
    }
}
