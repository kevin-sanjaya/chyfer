import { TripCoordinate } from './trip-coordinate';

export class Trip {
    private id: string;
    private start_time: string;
    private end_time: string;
    private coords: TripCoordinate[];

    constructor(trip: Trip) {
        this.id = `#${Math.floor(Math.random() * 90000) + 10000}`;
        this.start_time = trip.start_time;
        this.end_time = trip.end_time;
        this.coords = trip.coords.map(coords => new TripCoordinate(coords));
    }

    public get tripId(): string {
        return this.id;
    }

    public get coordinates(): TripCoordinate[] {
        return this.coords;
    }

    public get startTime(): string {
        return this.start_time;
    }

    public get endTime(): string {
        return this.end_time;
    }
}
