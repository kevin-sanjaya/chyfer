import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { RootComponent } from './root.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { TripListComponent } from './trip-list/trip-list.component';
import { AboutComponent } from './about/about.component';

import { TripService } from './services/trip.service';

@NgModule({
  declarations: [
    RootComponent,
    DashboardComponent,
    NavigationBarComponent,
    TripListComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [TripService],
  bootstrap: [RootComponent]
})

export class AppModule { }
