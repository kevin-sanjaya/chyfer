import { Component, OnInit } from '@angular/core';
import { TripService } from '../services/trip.service';
import { Trip } from '../models/trip';
declare let L;

@Component({
  selector: 'app-trip-list',
  templateUrl: './trip-list.component.html',
  styleUrls: ['./trip-list.component.css']
})
export class TripListComponent implements OnInit {
  private interactiveMap: any; // Leaflet Map
  private leafletMap: any = L; // Leaflet Library

  public tripData: Trip[];
  public totalPage: number[] = [];
  public showedPage = 0;
  public activePage = 1;
  public isLoading = true;
  public tripId: string;
  public totalDistance: string;
  public averageSpeed: string;

  constructor(private tripService: TripService) { }

  ngOnInit() {
    this.tripService.getTripData().subscribe((data: Trip[]) => {
      let pagination = 0;
      let pageNumber = 0;
      this.tripData = data.map(trip => {
        pagination += 1;
        if (pagination % 10 === 0) {
          this.totalPage.push(pageNumber += 1);
        }
        return new Trip(trip);
      });
      if (this.totalPage.length * 10 < this.tripData.length) {
        this.totalPage.push(this.totalPage.length + 1);
      }
      this.isLoading = false;
    });
  }

  public changePagination(previous: boolean): void {
    previous ? this.showedPage -= 10 : this.showedPage += 10;
  }

  public changeActivePage(page: number): void {
    this.activePage = page;
  }

  public getTripDuration(startTime: string, endTime: string): string {
    return ((new Date(endTime).getTime() - new Date(startTime).getTime()) / 60000).toFixed(0);
  }

  public updateTripDetail(trip: Trip): void {
    this.renderMap();
    this.tripId = trip.tripId;
    this.setMapCoordinate(trip);
    this.printTripOnMap(trip);
  }

  public downloadJSONFile(): void {

  }

  private renderMap() {
    if (!this.interactiveMap) {
      this.interactiveMap = this.leafletMap.map('map');

      this.leafletMap.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'YOUR_ACCESS_TOKEN_HERE'
      }).addTo(this.interactiveMap);
    }
  }

  private setMapCoordinate(trip: Trip): void {
    this.interactiveMap.setView([trip.coordinates[0].lattitude, trip.coordinates[0].longitude], 9);
  }

  private printTripOnMap(trip: Trip): void {
    this.interactiveMap.eachLayer(layer => {
      if (layer._path !== undefined) {
        this.interactiveMap.removeLayer(layer);
      }
    });
    const latlngs = [];
    const distance = [];
    let speed = 0;

    trip.coordinates.forEach((coords, index) => {
      latlngs.push([coords.lattitude, coords.longitude]);
      speed += coords.speeds;
      distance.push(coords.distance);
    });

    this.totalDistance = (Math.max(...distance) - Math.min(...distance)).toFixed(2);
    this.averageSpeed = (speed / trip.coordinates.length).toFixed(2);

    this.leafletMap.polyline(latlngs, { color: '#343A40', weight: 2 }).addTo(this.interactiveMap);
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 500);
  }

}

