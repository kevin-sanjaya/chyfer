import { Component, OnInit } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {
  @Output() public switchContentEvent = new EventEmitter<{selectedContent: string}>();
  public activeContent = 'dashboard';

  constructor() { }

  ngOnInit() { }

  switchContent(content): void {
    this.activeContent = content;
    this.switchContentEvent.emit({selectedContent: content});
  }

}
