import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NavigationBarComponent } from './navigation-bar.component';

describe('NavigationBar Component', () => {
    let component: NavigationBarComponent;
    let fixture: ComponentFixture<NavigationBarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NavigationBarComponent],
    })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NavigationBarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should have 3 navigation item', () => {
        const navItems: any[] = fixture.debugElement.queryAll(By.css('.nav-link'));
        const expectedNavItems = ['Dashboard', 'Dashcam Data Bank', 'About'];
        let index = 0;
        navItems.forEach(element => {
            expect(element.nativeElement.textContent).toBe(expectedNavItems[index]);
            index++;
        });
    });
});
