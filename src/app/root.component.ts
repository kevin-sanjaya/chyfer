import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent {
  public activeContent = 'dashboard';

  public switchContent(content): void {
    this.activeContent = content.selectedContent;
  }
}
